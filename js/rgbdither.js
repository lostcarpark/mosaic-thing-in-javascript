export class RgbDither {
  constructor(colors) {
    this.colors = colors;
  }
  closestMatch(rgb) {
    var min = -1;
    var closest;
    this.colors.forEach(
      function (color) {
        var checkbox = document.getElementById("color-" + color.id); // Check if color is enabled.
        if (checkbox.checked) {
          var distance =
            ((color.r - this.r) * 0.3) ** 2 +
            ((color.g - this.g) * 0.59) ** 2 +
            ((color.b - this.b) * 0.11) ** 2;
        }
        if (distance < min || min == -1) {
          min = distance;
          closest = color;
        }
      }.bind(rgb)
    );
    return closest;
  }

  getPixelError(original, matched) {
    return original.subtract(matched);
  }

  applyPixelError(destination, error) {
    return destination.add(error);
  }

  scaleError(error, ratio) {
    return error.scale(ratio);
  }

  /*
   * Simple quantization to closest color with no error correction.
   */
  ditherClosestMatch(pixels) {
    var maxY = pixels.length;
    for (var y = 0; y < maxY; y++) {
      var maxX = pixels[y].length;
      for (var x = 0; x < maxX; x++) {
        var color = this.closestMatch(pixels[y][x]);
        //color.count++;
        pixels[y][x] = color;
      }
    }
  }

  /*
   * Simple dithering just pushes error to next pixel.
   */
  ditherSimple(pixels) {
    var maxY = pixels.length;
    for (var y = 0; y < maxY; y++) {
      var maxX = pixels[y].length;
      for (var x = 0; x < maxX; x++) {
        var color = this.closestMatch(pixels[y][x]);
        var error = pixels[y][x].subtract(color);
        //color.count++;
        pixels[y][x] = color;
        if (x + 1 in pixels[y]) {
          pixels[y][x + 1] = this.applyPixelError(
            pixels[y][x + 1],
            this.scaleError(error, 0.5)
          );
        }
      }
    }
  }

  /*
   * Floyd-Steinberg dithering pushes error to surrounding pixels.
   */
  ditherFloydSteinberg(pixels) {
    var maxY = pixels.length;
    for (var y = 0; y < maxY; y++) {
      var maxX = pixels[y].length;
      for (var x = 0; x < maxX; x++) {
        var color = this.closestMatch(pixels[y][x]);
        var error = pixels[y][x].subtract(color);
        //color.count++;
        pixels[y][x] = color;
        if (x + 1 in pixels[y]) {
          pixels[y][x + 1] = pixels[y][x + 1].add(error.scale(7 / 16));
        }
        if (y + 1 in pixels) {
          if (x - 1 in pixels[y + 1]) {
            pixels[y + 1][x - 1] = pixels[y + 1][x - 1].add(
              error.scale(3 / 16)
            );
          }
          pixels[y + 1][x] = pixels[y + 1][x].add(error.scale(5 / 16));
          if (x + 1 in pixels[y + 1]) {
            pixels[y + 1][x + 1] = pixels[y + 1][x + 1].add(
              error.scale(1 / 16)
            );
          }
        }
      }
    }
  }

  /*
   * Jarvis-Judice-Ninke dithering pushes error over the next two rows.
   */
  ditherJarvisJudisNinke(pixels) {
    var maxY = pixels.length;
    for (var y = 0; y < maxY; y++) {
      var maxX = pixels[y].length;
      for (var x = 0; x < maxX; x++) {
        var color = this.closestMatch(pixels[y][x]);
        var error = pixels[y][x].subtract(color);
        //color.count++;
        pixels[y][x] = color;
        var seven = error.scale(7 / 48);
        var five = error.scale(5 / 48);
        var three = error.scale(3 / 48);
        var one = error.scale(1 / 48);
        if (x + 1 in pixels[y]) {
          pixels[y][x + 1] = pixels[y][x + 1].add(seven);
        }
        if (x + 2 in pixels[y]) {
          pixels[y][x + 2] = pixels[y][x + 2].add(five);
        }
        if (y + 1 in pixels) {
          if (x - 2 in pixels[y + 1]) {
            pixels[y + 1][x - 2] = pixels[y + 1][x - 2].add(three);
          }
          if (x - 1 in pixels[y + 1]) {
            pixels[y + 1][x - 1] = pixels[y + 1][x - 1].add(five);
          }
          pixels[y + 1][x] = pixels[y + 1][x].add(seven);
          if (x + 1 in pixels[y + 1]) {
            pixels[y + 1][x + 1] = pixels[y + 1][x + 1].add(five);
          }
          if (x + 2 in pixels[y + 1]) {
            pixels[y + 1][x + 2] = pixels[y + 1][x + 2].add(three);
          }
        }
        if (y + 2 in pixels) {
          if (x - 2 in pixels[y + 2]) {
            pixels[y + 2][x - 2] = pixels[y + 2][x - 2].add(one);
          }
          if (x - 1 in pixels[y + 2]) {
            pixels[y + 2][x - 1] = pixels[y + 2][x - 1].add(three);
          }
          pixels[y + 2][x] = pixels[y + 2][x].add(five);
          if (x + 1 in pixels[y + 2]) {
            pixels[y + 2][x + 1] = pixels[y + 2][x + 1].add(three);
          }
          if (x + 2 in pixels[y + 2]) {
            pixels[y + 2][x + 2] = pixels[y + 2][x + 2].add(one);
          }
        }
      }
    }
  }

  dither(method, pixels) {
    switch (method) {
      case "simple":
        this.ditherSimple(pixels);
        break;
      case "floyd_steinberg":
        this.ditherFloydSteinberg(pixels);
        break;
      case "jarvis_judice_ninke":
        this.ditherJarvisJudisNinke(pixels);
        break;
      default:
        this.ditherClosestMatch(pixels);
        break;
    }
  }
}
