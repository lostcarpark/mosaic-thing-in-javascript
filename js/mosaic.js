import { Color } from "./rgbcolor.js";
import { RgbDither } from "./rgbdither.js";

const colorUrl = "../data/colors.json";

let colors = [];
let colorsByName = [];
let colorCheckboxes = [];
let pixels = [];
let tiles = [];

const loadFile = function (event) {
  const image = document.getElementById("output");
  image.src = URL.createObjectURL(event.target.files[0]);

  if (image.complete) {
    rescaleImage(event);
  } else {
    image.addEventListener("load", rescaleImage);
  }
};

const getColorsAndRescale = function (event) {
  colors = getColors();
  rescaleImage(event);
};

const rescaleImage = function (event) {
  // First check if event is from color checkbox, and save value.
  if (event.target.type == "checkbox") {
    const matches = /^color-(\d+)/.exec(event.target.name);
    if (matches) {
      // Set the array index for the color ID to the state of the checkbox.
      colorCheckboxes[matches[1]] = event.target.checked;
      localStorage.setItem("color-" + matches[1], event.target.checked ? 1 : 0);
    }
  }
  const canvas = document.getElementById("canvas");
  const ctx = canvas.getContext("2d");
  const image = document.getElementById("output");
  canvas.width = document.getElementById("width").value;

  // set size proportional to image
  canvas.height = canvas.width * (image.height / image.width);

  smoothScaleImage(image, canvas);

  pixels = [];
  for (let y = 0; y < canvas.height; y++) {
    pixels[y] = [];
    for (let x = 0; x < canvas.width; x++) {
      const data = ctx.getImageData(x, y, 1, 1).data;
      pixels[y][x] = new Color(data[0], data[1], data[2]);
    }
  }

  colors.forEach(function (color) {
    color.count = 0;
  });

  countActiveColors();

  let model = null;
  const modelRadios = document.getElementsByName("model");
  for (let i = 0, length = modelRadios.length; i < length; i++) {
    if (modelRadios[i].checked) {
      model = modelRadios[i].value;
      break;
    }
  }

  const dither = new RgbDither(colors);
  let ditherMethod;
  const ditherRadios = document.getElementsByName("dither");
  for (let i = 0, length = ditherRadios.length; i < length; i++) {
    if (ditherRadios[i].checked) {
      ditherMethod = ditherRadios[i].value;
      break;
    }
  }
  dither.dither(ditherMethod, pixels);

  makeTiles(pixels, colors);

  showPixels("Mosaic", pixels, document.getElementById("grid"), "cell");
  showColors(colors, document.getElementById("counts"));

  showTiles(document.getElementById("tiles"));
};

function smoothScaleImage(image, canvas) {
  const ctx = canvas.getContext("2d");
  if (ctx.hasOwnProperty("imageSmoothingQuality")) {
    ctx.imageSmoothingQuality = "high";
    ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
  } else {
    // imageSmoothingQuality not supported. Progressively resize image until fits.
    if (
      image.naturalWidth <= canvas.width &&
      image.naturalHeight <= canvas.height
    ) {
      // Enlarge the image: no inbetween-steps needed:
      ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
    } else {
      // Shrink the image: inbetween-steps make ik look smooth:
      const oc = document.createElement("canvas");
      const octx = oc.getContext("2d");
      let cur = {
        width: Math.round(image.naturalWidth / 2),
        height: Math.round(image.naturalHeight / 2),
      };
      oc.width = cur.width;
      oc.height = cur.height;
      octx.drawImage(image, 0, 0, cur.width, cur.height);

      while (cur.width / 2 > canvas.width) {
        octx.drawImage(
          oc,
          0,
          0,
          cur.width,
          cur.height,
          0,
          0,
          cur.width / 2,
          cur.height / 2
        );
        cur = {
          width: Math.round(cur.width / 2),
          height: Math.round(cur.height / 2),
        };
      }
      ctx.drawImage(
        oc,
        0,
        0,
        cur.width,
        cur.height,
        0,
        0,
        canvas.width,
        canvas.height
      );
    }
  }
}

const selectAll = function (event) {
  let count = 0;
  document
    .getElementById("color-checkboxes")
    .querySelectorAll("input")
    .forEach(function (checkBox) {
      checkBox.checked = true;
      count++;
    });
  showActiveCount(count);
  rescaleImage(event);
};

const selectRange = function (min, max) {
  let count = 0;
  allColors.forEach((color) => {
    const check = document.getElementById("color-" + color.id);
    if (check) {
      if (color.from <= max && color.to >= min) {
        check.checked = true;
        count++;
      } else {
        check.checked = false;
      }
    }
  });
  showActiveCount(count);
  rescaleImage(event);
};

const selectModern = function (event) {
  let count = 0;
  allColors.forEach((color) => {
    const check = document.getElementById("color-" + color.id);
    if (check) {
      if (color.to >= 2020) {
        check.checked = true;
        count++;
      } else {
        check.checked = false;
      }
    }
  });
  showActiveCount(count);
  rescaleImage(event);
};

const selectNone = function (event) {
  document
    .getElementById("color-checkboxes")
    .querySelectorAll("input")
    .forEach(function (checkBox) {
      checkBox.checked = false;
    });
  showActiveCount(0);
};

const selectColors = function (event) {
  switch (event.target.id) {
    case "select-all":
      selectAll(event);
      break;
    case "select-none":
      selectNone(event);
      break;
    case "select-1970s":
      selectRange(1970, 1979);
      break;
    case "select-1980s":
      selectRange(1980, 1999);
      break;
    case "select-1990s":
      selectRange(1990, 1999);
      break;
    case "select-2000s":
      selectRange(2000, 2009);
      break;
    case "select-2010s":
      selectRange(2010, 2019);
      break;
    case "select-modern":
      selectModern(event);
      break;
  }
};

const setElementSize = function (event) {
  const cellSize = getCellSize();

  const rule = findRule(".cell");
  rule.style.width = cellSize + "px";
  rule.style.height = cellSize + "px";

  const grid = document.getElementById("grid");
  const gridDiv = grid.querySelector(".mosaic-table");
  gridDiv.style.width = pixels[0].length * cellSize + "px";
};

function getCellSize() {
  const radios = document.getElementsByName("element-size");
  for (let i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
      return radios[i].value;
    }
  }
}

const moveHoverBox = function (event) {
  const hover = document.getElementById("hover");
  hover.innerHTML = "";
  hover.style.position = "absolute";
  hover.style.left = event.pageX + 8 + "px";
  hover.style.top = event.pageY + 8 + "px";
  let color = null;
  let x = null;
  let y = null;
  for (let cssClass of event.target.classList) {
    // If "color-" class set.
    const colorResult = /^color-([\d]+)$/.exec(cssClass);
    if (colorResult) {
      color = colors[colorResult[1]];
    }
    const xResult = /^x-([\d]+)$/.exec(cssClass);
    if (xResult) {
      x = parseInt(xResult[1]);
    }
    const yResult = /^y-([\d]+)$/.exec(cssClass);
    if (yResult) {
      y = parseInt(yResult[1]);
    }
  }
  if (x != null) {
    const tileSize = parseInt(document.getElementById("tile-size").value);
    const tilesAcross = Math.floor(pixels[0].length / tileSize);
    const tileCol = Math.floor(x / tileSize);
    const tileRow = Math.floor(y / tileSize);
    const tile = tileRow * tilesAcross + tileCol + 1;
    const divCoord = document.createElement("div");
    divCoord.appendChild(
      document.createTextNode("Row: " + (y + 1) + ", Col: " + (x + 1))
    );
    hover.appendChild(divCoord);
    if (tile != null) {
      const divTile = document.createElement("div");
      divTile.appendChild(document.createTextNode("Tile: " + tile));
      hover.appendChild(divTile);

      const highlightSize = getCellSize() * tileSize;
      const grid = document.getElementById("grid");
      const gridDiv = grid.querySelector(".mosaic-table");
      const ofs = getOffset(gridDiv);
      positionElement(
        "highlight-tile-top",
        ofs.left + highlightSize * tileCol,
        ofs.top + highlightSize * tileRow,
        highlightSize + "px",
        "1px"
      );
      positionElement(
        "highlight-tile-left",
        ofs.left + highlightSize * tileCol,
        ofs.top + highlightSize * tileRow,
        "1px",
        highlightSize + "px"
      );
      positionElement(
        "highlight-tile-bottom",
        ofs.left + highlightSize * tileCol,
        ofs.top + highlightSize * (tileRow + 1) - 1,
        highlightSize + "px",
        "1px"
      );
      positionElement(
        "highlight-tile-right",
        ofs.left + highlightSize * (tileCol + 1) - 1,
        ofs.top + highlightSize * tileRow,
        "1px",
        highlightSize + "px"
      );
    }
  }
  const divId = document.createElement("div");
  divId.appendChild(document.createTextNode("ID: " + color.id));
  hover.appendChild(divId);
  const divName = document.createElement("div");
  divName.appendChild(document.createTextNode(color.name));
  hover.appendChild(divName);
  const divCode = document.createElement("div");
  divCode.appendChild(document.createTextNode(color.code));
  hover.appendChild(divCode);
  const divHex = document.createElement("div");
  divHex.appendChild(document.createTextNode(color.hex));
  hover.appendChild(divHex);
  const divYears = document.createElement("div");
  divYears.appendChild(document.createTextNode(color.from + ' to ' + color.to));
  hover.appendChild(divYears);
  if ("count" in color) {
    const divCount = document.createElement("div");
    divCount.appendChild(
      document.createTextNode("Quantity in image: " + color.count)
    );
    hover.appendChild(divCount);
  }
  hover.style.display = "block";
};

const hideHoverBox = function (event) {
  const hover = document.getElementById("hover");
  hover.style.display = "none";
  const divHighlightTop = document.getElementById("highlight-tile-top");
  divHighlightTop.style.display = "none";
  const divHighlightLeft = document.getElementById("highlight-tile-left");
  divHighlightLeft.style.display = "none";
  const divHighlightBottom = document.getElementById("highlight-tile-bottom");
  divHighlightBottom.style.display = "none";
  const divHighlightRight = document.getElementById("highlight-tile-right");
  divHighlightRight.style.display = "none";
};

function positionElement(id, left, top, width, height) {
  const element = document.getElementById(id);
  element.style.position = "absolute";
  element.style.left = left;
  element.style.top = top;
  element.style.width = width;
  element.style.height = height;
  element.style.display = "block";
}

function getOffset(el) {
  const rect = el.getBoundingClientRect();
  return {
    left: rect.left + window.scrollX,
    top: rect.top + window.scrollY,
  };
}

function makeTiles(pixels, colors) {
  const tileSize = parseInt(document.getElementById("tile-size").value);

  tiles = [];
  let tileNum = 1;
  let row = 1;
  let col = 1;
  for (let y = 0; y < pixels.length; y += tileSize) {
    if (y in pixels) {
      for (let x = 0; x < pixels[y].length; x += tileSize) {
        if (x in pixels[y]) {
          tiles[tileNum] = makeTile(pixels, colors, x, y, tileSize, tileNum);
          tiles[tileNum].num = tileNum;
          tiles[tileNum].row = row;
          tiles[tileNum].col = col;
          tileNum++;
          col++;
        }
      }
      row++;
      col = 1;
    }
  }
}

function makeTile(pixels, colors, orgX, orgY, tileSize, tileNum) {
  const newPixels = [];
  const newColors = [];
  for (let y = 0; y < tileSize; y++) {
    if (orgY + y in pixels) {
      newPixels[y] = [];
      for (let x = 0; x < tileSize; x++) {
        if (orgX + x in pixels[orgY + y]) {
          const color = pixels[orgY + y][orgX + x];
          if (!(color.id in newColors)) {
            newColors[color.id] = color.clone();
            newColors[color.id].count = 0;
          }
          newPixels[y][x] = newColors[color.id]; // Set the pixel on the tile.
        }
      }
    }
  }
  return { pixels: newPixels, colors: newColors };
}

function showTiles(tilesDiv) {
  tilesDiv.innerHTML = "";
  tiles.forEach(function (tile) {
    const tileDiv = document.createElement("div");
    tileDiv.classList.add("section");
    const grid = document.createElement("div");
    showPixels(
      "Tile " + tile.num + " (row " + tile.row + ", column " + tile.col + ")",
      tile.pixels,
      grid,
      "tile-cell",
      true
    );
    tileDiv.appendChild(grid);
    const counts = document.createElement("div");
    showColors(tile.colors, counts);
    tileDiv.appendChild(counts);
    tilesDiv.appendChild(tileDiv);
  });
}

function applyColor(object, color) {
  object.style.backgroundColor = color.hex;
  if (color.r * 0.3 + color.g * 0.59 + color.b * 0.11 > 127) {
    object.style.color = "#000";
  } else {
    object.style.color = "#fff";
  }
  object.classList.add("color-" + color.id);
  object.addEventListener("mouseover", function (event) {
    moveHoverBox(event);
  });
  object.addEventListener("mouseout", function (event) {
    hideHoverBox(event);
  });
}

function showPixels(title, pixels, grid, cellClass, showIds = false) {
  if (pixels.length == 0) {
    return false; // No image to show.
  }
  const rule = findRule("." + cellClass);
  const result = /([\d]+)px/.exec(rule.style.width);
  if (result) {
    const cellSize = result[1];

    grid.innerHTML = "";
    const h2 = document.createElement("h2");
    h2.appendChild(document.createTextNode(title));
    const table = document.createElement("div");
    table.classList.add("mosaic-table");
    table.style.width = pixels[0].length * cellSize + "px";
    grid.appendChild(h2);
    for (let y = 0; y < pixels.length; y++) {
      const line = document.createElement("div");
      line.classList.add("mosaic-row");
      for (let x = 0; x < pixels[y].length; x++) {
        const color = pixels[y][x];
        color.count++;
        const cell = document.createElement("div");
        cell.classList.add("mosaic-col");
        cell.classList.add(cellClass);
        cell.classList.add("x-" + x);
        cell.classList.add("y-" + y);
        applyColor(cell, color);
        if (showIds) {
          cell.appendChild(document.createTextNode(color.code));
        }
        line.appendChild(cell);
      }
      table.appendChild(line);
    }
    grid.appendChild(table);
  }
}

function findRule(selector) {
  const ruleList = document.styleSheets[0].cssRules;
  for (let i = 0; i < ruleList.length; i++) {
    if (ruleList[i].selectorText == selector) {
      return ruleList[i];
    }
  }
}

function showColors(colors, counts) {
  let unique = 0;
  let total = 0;
  counts.innerHTML = "";
  const table = document.createElement("table");
  const thead = table.createTHead();
  const headrow = thead.insertRow();
  const headings = ["ID", "Code", "Name", "RGB", "Quantity"];
  for (let key of headings) {
    const th = document.createElement("th");
    th.appendChild(document.createTextNode(key));
    headrow.appendChild(th);
  }
  const tbody = table.createTBody();
  colorsByName.forEach(function (namedColor) {
    if (namedColor.id in colors) {
      const color = colors[namedColor.id];
      if ("count" in color && color.count > 0) {
        unique++;
        total += color.count;
        const row = tbody.insertRow();
        const idCell = row.insertCell();
        idCell.appendChild(document.createTextNode(color.id));
        const codeCell = row.insertCell();
        applyColor(codeCell, color);
        codeCell.appendChild(document.createTextNode(color.code));
        const nameCell = row.insertCell();
        applyColor(nameCell, color);
        nameCell.appendChild(document.createTextNode(color.name));
        const rgbCell = row.insertCell();
        rgbCell.appendChild(document.createTextNode(color.hex));
        const qtyCell = row.insertCell();
        qtyCell.appendChild(document.createTextNode(color.count));
        qtyCell.align = "right";
      }
    }
  });
  const tfoot = table.createTFoot();
  const totalRow = tfoot.insertRow();
  const totalLabelCell = totalRow.insertCell();
  totalLabelCell.appendChild(document.createTextNode("Total elements"));
  totalLabelCell.colSpan = 4;
  const totalValueCell = totalRow.insertCell();
  totalValueCell.appendChild(document.createTextNode(total));
  totalValueCell.align = "right";

  const uniqueRow = tfoot.insertRow();
  const uniqueLabelCell = uniqueRow.insertCell();
  uniqueLabelCell.appendChild(document.createTextNode("Unique colors"));
  uniqueLabelCell.colSpan = 4;
  const uniqueValueCell = uniqueRow.insertCell();
  uniqueValueCell.appendChild(document.createTextNode(unique));
  uniqueValueCell.align = "right";

  counts.appendChild(table);
}

function getColorCookies() {
  const checkboxes = [];
  allColors.forEach(function (rawCol) {
    if (rawCol.id >= 0 && rawCol.id < 9999) {
      const checked = localStorage.getItem("color-" + rawCol.id);
      if (checked == null) {
        checkboxes[rawCol.id] = true;
      } else {
        checkboxes[rawCol.id] = checked == 1 ? true : false;
      }
    }
  });
  return checkboxes;
}

function getColors() {
  let count = 0;
  const colors = [];
  colorsByName = [];
  const trans = document.getElementById("trans");
  const pearl = document.getElementById("pearl");
  const metallic = document.getElementById("metallic");
  const modulex = document.getElementById("modulex");
  const duplo = document.getElementById("duplo");
  const fabuland = document.getElementById("fabuland");
  const clikits = document.getElementById("clikits");
  const ho = document.getElementById("ho");
  const vintage = document.getElementById("vintage");
  // Get div for color checkboxes and remove contents.
  const checkboxes = document.getElementById("color-checkboxes");
  checkboxes.innerHTML = "";
  allColors.forEach(function (rawCol) {
    if (
      rawCol.id >= 0 &&
      rawCol.id < 9999 &&
      (!/Pearl/i.test(rawCol.name) || pearl.checked) &&
      (!rawCol.is_metallic || metallic.checked) &&
      (!rawCol.is_trans || trans.checked) &&
      (!/Modulex/i.test(rawCol.name) || modulex.checked) &&
      (!/^Duplo/i.test(rawCol.name) || duplo.checked) &&
      (!/^Fabuland/i.test(rawCol.name) || fabuland.checked) &&
      (!/^Clikits/i.test(rawCol.name) || clikits.checked) &&
      (!/^HO/i.test(rawCol.name) || ho.checked) &&
      (!/Vintage|Fabuland Orange/i.test(rawCol.name) || vintage.checked) // Previously treated colors over 1000 as vintage, but that excluded modern colors like Coral.
    ) {
      const color = Color.fromHex(
        rawCol.rgb,
        rawCol.id,
        rawCol.name,
        rawCol.code,
        rawCol.from,
        rawCol.to
      );
      colors[color.id] = color;
      colorsByName[color.id] = color;
      count++;
    }
  });
  colorsByName.sort((a, b) => {
    if (a.name > b.name) {
      return 1;
    }
    if (a.name < b.name) {
      return -1;
    }
    return 0;
  });
  colorsByName.forEach(function (color) {
    // Add checkbox for color.
    addCheckbox(checkboxes, color);
  });
  showActiveCount(count);
  return colors;
}

function addCheckbox(checkboxes, color) {
  const name = "color-" + color.id;
  const div = document.createElement("div");
  div.classList.add("checkbox");
  const check = document.createElement("input");
  check.type = "checkBox";
  if (color.id in colorCheckboxes) {
    check.checked = colorCheckboxes[color.id];
  } else {
    colorCheckboxes[color.id] = true;
    check.checked = true;
  }
  check.id = name;
  check.name = name;
  check.addEventListener("change", function (event) {
    rescaleImage(event);
  });
  div.appendChild(check);
  const label = document.createElement("label");
  label.for = name;
  label.appendChild(document.createTextNode(color.name));
  applyColor(label, color);
  div.appendChild(label);
  checkboxes.appendChild(div);
}

function countActiveColors() {
  let count = 0;
  document
    .getElementById("color-checkboxes")
    .querySelectorAll("input")
    .forEach(function (checkBox) {
      if (checkBox.checked) {
        count++;
      }
    });
  showActiveCount(count);
}

function showActiveCount(count) {
  const summary = document.getElementById("colors-summary");
  summary.innerHTML = "Total colors active: " + count;
}

function addEventHandlers() {
  document.getElementById("file").addEventListener("change", loadFile);
  const rescale = document.querySelectorAll(".rescale");
  console.log(rescale);
  rescale.forEach((el) => el.addEventListener("change", rescaleImage));
  document
    .querySelectorAll(".colors-rescale")
    .forEach((el) => el.addEventListener("change", getColorsAndRescale));
  document
    .querySelectorAll(".select")
    .forEach((el) => el.addEventListener("click", selectColors));
  document
    .querySelectorAll(".element-size")
    .forEach((el) => el.addEventListener("change", setElementSize));
}

addEventHandlers();
const response = await fetch(colorUrl);
if (!response.ok) {
  console.log("Error loading color data.", response.status);
}
const allColors = await response.json();
console.log(allColors);

colors = getColors();
colorCheckboxes = getColorCookies();
