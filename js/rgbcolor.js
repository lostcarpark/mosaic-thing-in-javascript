/**
 * Color class.
 * Functionality for RGB color arithmatic.
 */
export class Color {
  constructor(r, g, b, id = null, name = null, code = null, from = null, to = null) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.id = id;
    this.name = name;
    this.code = code;
    this.from = from;
    this.to = to;
  }
  add(a) {
    return new Color(this.r + a.r, this.g + a.g, this.b + a.b);
  }
  subtract(a) {
    return new Color(this.r - a.r, this.g - a.g, this.b - a.b);
  }
  scale(factor) {
    return new Color(this.r * factor, this.g * factor, this.g * factor);
  }
  static componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }
  get hex() {
    return (
      "#" +
      Color.componentToHex(this.r) +
      Color.componentToHex(this.g) +
      Color.componentToHex(this.b)
    );
  }
  static fromHex(hex, id = null, name = null, code = null, from = null, to = null) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result
      ? new Color(
          parseInt(result[1], 16),
          parseInt(result[2], 16),
          parseInt(result[3], 16),
          id,
          name,
          code,
          from,
          to
        )
      : null;
  }
  clone() {
    return new Color(this.r, this.g, this.b, this.id, this.name, this.code, this.from, this.to);
  }
}
