class HSL {
    constructor(rgb) {
        this.RGBtoHSL(rgb);
    }
    RGBtoHSL(rgb) {
        // Make r, g, and b fractions of 1
        var r = rgb.r / 255;
        var g = rgb.g / 255;
        var b = rgb.b / 255;
      
        // Find greatest and smallest channel values
        let cmin = Math.min(r,g,b),
            cmax = Math.max(r,g,b),
            delta = cmax - cmin,
            h = 0,
            s = 0,
            l = 0;
      
        // Calculate hue
        // No difference
        if (delta == 0)
          h = 0;
        // Red is max
        else if (cmax == r)
          h = ((g - b) / delta) % 6;
        // Green is max
        else if (cmax == g)
          h = (b - r) / delta + 2;
        // Blue is max
        else
          h = (r - g) / delta + 4;
      
        h = Math.round(h * 60);
          
        // Make negative hues positive behind 360°
        if (h < 0)
            h += 360;
      
        // Calculate lightness
        l = (cmax + cmin) / 2;
      
        // Calculate saturation
        s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
          
        // Multiply l and s by 100
        s = +(s * 100).toFixed(1);
        l = +(l * 100).toFixed(1);
      
        this.h = h;
        this.s = s;
        this.l = l;
    }
    calcDistance(to) {
        var h1 = this.h / 360, h2 = to.h / 360;
        var s1 = this.s / 100, s2 = to.s / 100;
        var l1 = this.l / 100, l2 = to.l / 100;
        return (Math.sin(h1) * s1 * l1 - Math.sin(h2) * s2 * l2) ** 2
             + (Math.cos(h1) * s1 * l1 - Math.cos(h2) * s2 * l2) ** 2
             + (l1 - l2) ** 2;
    }
    closestMatch() {
        var min = -1;
        var closest;
        var distance;
        colors.forEach(function(color) {
            var checkbox = document.getElementById('color-' + color.id); // Check if color is enabled.
            if (checkbox.checked) {
                distance = this.calcDistance(color.hsl);
                if (distance < min || min == -1) {
                    min = distance;
                    closest = color;
                }
            }
        }.bind(this));
        return closest;
    }
}